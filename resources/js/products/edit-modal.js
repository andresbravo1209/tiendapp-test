import React, {useEffect} from 'react';
import {
    Button,
    Col,
    Modal,
    Row,
} from "react-bootstrap";
import moment from "moment/moment";
import Input from "../components/fields/Input";
import {useForm} from "react-hook-form";
import { yupResolver } from '@hookform/resolvers/yup';
import SimpleSelect from "../components/fields/SimpleSelect";
import Number from "../components/fields/Number";
import TextArea from "../components/fields/TextArea";
import axios from "axios";
import swal from 'sweetalert';
import {sizeOptions} from "../config/constants";
import {productsValidationSchema} from "../validations/products";

moment.locale('es');
const EditModal = ({brandOptions,show, setShow, product, refresh = () => {}}) => {

    const handleClose = () => setShow(false);
    const { register, setValue, handleSubmit, formState: { errors }, } = useForm({
        resolver: yupResolver(productsValidationSchema)
    });

    useEffect(() => {
        if (!product) return;
        setValue('name', product.name);
        setValue('observations', product.observations);
        setValue('size', product.size);
        setValue('quantity', product.quantity);
        setValue('brand_id', product.brand? product.brand.reference : '');
    }, [product])

    const onSubmit = (data) => {
        axios.put(`/api/products/${product.id}`, data).then(
            response => {
                if (response.data.success) {
                    refresh(refresh => !refresh);
                    handleClose();
                    swal("Exito!", "Producto Actualizado!", "success");
            }
        }).catch(error => {
            //  Handling the response
            console.log(error, 'error')
            if (error.response.data.errors){
                let msg = 'Error de validación';
                const errors = error.response.data.errors;
                msg += ': ' + Object.values(errors).join(' ');
                swal({
                    title: "Ops!",
                    text: msg,
                    icon: "warning",
                });
            }
        });
    };

    return (
        <>
            <Modal
                size='lg'
                show={show}
                onHide={handleClose}
                backdrop="static"
                keyboard={false}
            >
                <Modal.Header closeButton>
                    <Modal.Title><b>Editar Producto: </b> {product.name}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <form>
                        <Row>
                            {/* <form onSubmit={handleSubmit(onSubmit)}>*/}
                            <Col>
                                <Input
                                    name='Nombre'
                                    label="Nombre del producto"
                                    field='name'
                                    register={register}
                                    defaultValue={product.name}
                                    required
                                />
                                {errors.name &&
                                <small className='text-danger'>{errors.name?.message}</small>}
                            </Col>
                        </Row>
                        <Row className='mt-2'>
                            <Col>
                                <TextArea
                                    name='Observaciones'
                                    label="Observaciones"
                                    field='observations'
                                    register={register}
                                    defaultValue={product.observations}
                                    required
                                />
                                {errors.observations &&
                                <small className='text-danger'>{errors.observations?.message}</small>}
                            </Col>
                        </Row>
                        <Row className='mt-2'>
                            <Col>
                                <SimpleSelect
                                    options={sizeOptions}
                                    defaultMessage='Selecciona una talla'
                                    defaultValue={product.size}
                                    name='Talla'
                                    label="Talla"
                                    field='size'
                                    register={register}
                                    required
                                />
                                {errors.size &&
                                <small className='text-danger'>{errors.size?.message}</small>}
                            </Col>
                        </Row>
                        <Row className='mt-2'>
                            <Col>
                                <Number
                                    defaultValue={product.quantity}
                                    name='Cantidad Disponible'
                                    label="Cantidad"
                                    field='quantity'
                                    register={register}
                                    required
                                />
                                {errors.quantity &&
                                <small className='text-danger'>{errors.quantity?.message}</small>}
                            </Col>
                        </Row>
                        <Row className='mt-2'>
                            <Col>
                                <SimpleSelect
                                    defaultMessage='Selecciona una marca'
                                    defaultValue={product.brand_id}
                                    options={brandOptions}
                                    name='Marcas'
                                    label="Selecciona una marca"
                                    field='brand_id'
                                    register={register}
                                    required
                                />
                                {errors.brand_id &&
                                <small className='text-danger'>{errors.brand_id?.message}</small>}
                            </Col>
                        </Row>
                        <Row>
                            <Col className='text-center'>
                                <Button className='mt-2' type='submit' onClick={handleSubmit(onSubmit)}>
                                    Editar
                                </Button>
                            </Col>
                        </Row>
                    </form>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="primary" onClick={handleClose}>Cerrar</Button>
                </Modal.Footer>
            </Modal>
        </>
    );
}

export default EditModal;
