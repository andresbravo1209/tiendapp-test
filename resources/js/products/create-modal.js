import React from 'react';
import {
    Button,
    Col,
    Modal,
    Row,
} from "react-bootstrap";
import moment from "moment/moment";
import Input from "../components/fields/Input";
import {useForm} from "react-hook-form";
import { yupResolver } from '@hookform/resolvers/yup';
import SimpleSelect from "../components/fields/SimpleSelect";
import Number from "../components/fields/Number";
import TextArea from "../components/fields/TextArea";
import axios from "axios";
import swal from 'sweetalert';
import {sizeOptions} from "../config/constants";
import {productsValidationSchema} from "../validations/products";

moment.locale('es');
const CreateModal = ({brandOptions,show, setShow, refresh = () => {}}) => {

    const handleClose = () => setShow(false);
    const {
        register,
        handleSubmit,
        formState: { errors },
    } = useForm({
        resolver: yupResolver(productsValidationSchema)
    });

    const onSubmit = (data) => {
        axios.post(`/api/products`, data).then(
            response => {
                if (response.data.success) {
                    refresh(refresh => !refresh);
                    handleClose();
                    swal("Exito!", "Producto Creado!", "success");
                }
            }).catch(error => {
            //  Handling the response
            if (error.response.data.errors){
                let msg = 'Error de validación';
                const errors = error.response.data.errors;
                msg += ': ' + Object.values(errors).join(' ');
                swal({
                    title: "Ops!",
                    text: msg,
                    icon: "warning",
                });
            }
            if (!error.response.data.success){
                swal({
                    title: "Ops!",
                    text: error.response.data.error? error.response.data.error.error : '',
                    icon: "warning",
                });
            }
        });
    };

    return (
        <>
            <Modal
                size='lg'
                show={show}
                onHide={handleClose}
                backdrop="static"
                keyboard={false}
            >
                <Modal.Header closeButton>
                    <Modal.Title><b>Crear Producto: </b></Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <form>
                        <Row>
                            {/* <form onSubmit={handleSubmit(onSubmit)}>*/}
                            <Col>
                                <Input
                                    name='Nombre'
                                    label="Nombre del producto"
                                    field='name'
                                    register={register}
                                    defaultValue={''}
                                    required
                                />
                                {errors.name &&
                                <small className='text-danger'>{errors.name?.message}</small>}
                            </Col>
                        </Row>
                        <Row className='mt-2'>
                            <Col>
                                <TextArea
                                    name='Observaciones'
                                    label="Observaciones"
                                    field='observations'
                                    register={register}
                                    defaultValue={''}
                                    required
                                />
                                {errors.observations &&
                                <small className='text-danger'>{errors.observations?.message}</small>}
                            </Col>
                        </Row>
                        <Row className='mt-2'>
                            <Col>
                                <SimpleSelect
                                    options={sizeOptions}
                                    defaultMessage='Selecciona una talla'
                                    defaultValue={''}
                                    name='Talla'
                                    label="Talla"
                                    field='size'
                                    register={register}
                                    required
                                />
                                {errors.size &&
                                <small className='text-danger'>{errors.size?.message}</small>}
                            </Col>
                        </Row>
                        <Row className='mt-2'>
                            <Col>
                                <Number
                                    defaultValue={''}
                                    name='Cantidad Disponible'
                                    label="Cantidad"
                                    field='quantity'
                                    register={register}
                                    required
                                />
                                {errors.quantity &&
                                <small className='text-danger'>{errors.quantity?.message}</small>}
                            </Col>
                        </Row>
                        <Row className='mt-2'>
                            <Col>
                                <SimpleSelect
                                    defaultMessage='Selecciona una marca'
                                    defaultValue={''}
                                    options={brandOptions}
                                    name='Marcas'
                                    label="Selecciona una marca"
                                    field='brand_id'
                                    register={register}
                                    required
                                />
                                {errors.brand_id &&
                                <small className='text-danger'>{errors.brand_id?.message}</small>}
                            </Col>
                        </Row>
                        <Row>
                            <Col className='text-center'>
                                <Button className='mt-2' type='submit' onClick={handleSubmit(onSubmit)}>
                                    Crear
                                </Button>
                            </Col>
                        </Row>
                    </form>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="primary" onClick={handleClose}>Cerrar</Button>
                </Modal.Footer>
            </Modal>
        </>
    );
}

export default CreateModal;
