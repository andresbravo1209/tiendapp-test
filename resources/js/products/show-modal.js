import React from 'react';
import {
    Button,
    Card,
    Col,
    Modal,
    Row
} from "react-bootstrap";
import moment from "moment/moment";

moment.locale('es');
const ShowModal = ({show, setShow, product}) => {

    const formatDate = timestamp => moment(timestamp).format("dddd, MMMM Do YYYY");
    const formatHour = timestamp => moment(timestamp).format("h:mm:ss a");

    const handleClose = () => setShow(false);

    return (
        <>
            <Modal
                show={show}
                onHide={handleClose}
                backdrop="static"
                keyboard={false}
            >
                <Modal.Header closeButton>
                    <Modal.Title><b>Producto:</b> {product.name}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Row>
                        <Col>
                            <Card className="text-center">
                                <Card.Header>Información</Card.Header>
                                <Card.Body>
                                    <small><b>Cantidad: </b></small><small>{product.quantity}</small>
                                    <br/>
                                    <small><b>Comentarios: </b></small><small>{product.observations}</small>
                                    <br/>
                                    <small><b>Talla: </b></small><small>{product.size}</small>
                                    <br/>
                                    {product.brand && <>
                                        <small><b>Marca: </b>
                                        </small><small>{product.brand.name}</small>
                                    </>}
                                </Card.Body>
                                <Card.Footer className="text-muted">
                                    <small>
                                        Fecha: {formatDate(product.embark_at)}
                                    </small>
                                    <br/>
                                    <small>
                                        Hora: {formatHour(product.embark_at) }
                                    </small>
                                </Card.Footer>
                            </Card>
                        </Col>
                    </Row>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="primary" onClick={handleClose}>Cerrar</Button>
                </Modal.Footer>
            </Modal>
        </>
    );
}

export default ShowModal;
