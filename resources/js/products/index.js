import React, {useEffect, useState} from 'react';
import ReactDOM from 'react-dom';
import axios from "axios";
import {
    Button,
    ButtonGroup,
    Col,
    Container,
    Row,
    Table,
} from "react-bootstrap";
import NavBar from "../components/NavBar";
import moment from "moment/moment";
import {
    BsEyeFill,
    BsPencilSquare,
    BsXCircleFill
} from "react-icons/bs";
import ShowModal from "./show-modal";
import EditModal from "./edit-modal";
import swal from "sweetalert";
import CreateModal from "./create-modal";

moment.locale('es');
const ProductsIndex = () => {

    const [show, setShow] = useState(false);
    const [refresh, setRefresh] = useState(false);
    const [showEdit, setShowEdit] = useState(false);
    const [showCreate, setShowCreate] = useState(false);
    const [product, setProduct] = useState({});
    const [brandOptions, setBrandOptions] = useState({});

    const handleShow = (e, product) => {
        e.preventDefault();
        setProduct(product);
        setShow(true)
    };

    const handleShowEdit = (e, product) => {
        e.preventDefault();
        setProduct(product);
        setShowEdit(true)
    };

    const [products, setProducts] = useState([]);

    useEffect(() => {
        axios.get('/api/products').then(
            response => {
                if (response.data.success) {
                    setProducts(response.data.data.products)
                }
            });
        axios.get('/api/brands').then(
            response => {
                if (response.data.success) {
                    const brandOpts = response.data.data.brands.map(brand => ({
                        value: brand.reference,
                        label: brand.name,
                    }));
                    setBrandOptions(brandOpts)
                }
            });
        }, [refresh]);

    const DescriptionField = ({text}) => <td>{text}</td>;

    const onDelete = (e, product) => {
        e.preventDefault();
        swal({
            title: "Borrar Producto?",
            text: `Estas a punto de borrar el producto ${product.name}`,
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    axios.delete(`/api/products/${product.id}`).then(response =>
                    {
                        if (response.data.success){
                            setRefresh(refresh => !refresh);
                            swal("El producto ha sido borrado!", {
                                icon: "success",
                            });
                        }
                    }).catch(error => {
                        //  Handling the response (Show an error notification etc)
                        if (error.response) { // You can also check the status i.e 422
                            swal("Ups!", {
                                icon: "error",
                            });
                        }
                    });

                }
            });
    };

    return (
        <>
            <NavBar/>
            <Container fluid>
                <Row className='text-end mt-2'>
                    <Col>
                        <Button variant="primary" type='button' className='text-center' onClick={e => setShowCreate(true)}>
                            Crear Producto
                        </Button>
                    </Col>
                </Row>
                <Row className='text-center'>
                    <Col className='mt-5'>
                        {products.length?
                           <>
                                <Table responsive striped bordered hover>
                                    <thead>
                                    <tr>
                                        <th>identificador</th>
                                        <th>Nombre</th>
                                        <th>Marca</th>
                                        <th>Unidades disponibles</th>
                                        <th>Observaciones</th>
                                        <th>Acciones</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {products.map(product => (
                                        <tr key={product.id}>
                                            <DescriptionField text={product.id}/>
                                            <DescriptionField text={product.name}/>
                                            <DescriptionField text={product.brand.name}/>
                                            <DescriptionField text={product.quantity} />
                                            <DescriptionField text={product.observations} />
                                            <td>
                                                <ButtonGroup size="sm">
                                                    <Button onClick={e => handleShow(e, product)}>
                                                        <BsEyeFill/>
                                                    </Button>
                                                    <Button onClick={e => handleShowEdit(e, product)}>
                                                        <BsPencilSquare/>
                                                    </Button>
                                                    <Button onClick={e => onDelete(e, product)}>
                                                        <BsXCircleFill/>
                                                    </Button>
                                                </ButtonGroup>
                                            </td>
                                        </tr>
                                    ))}
                                    </tbody>
                                </Table>
                               <ShowModal
                                   show={show}
                                   setShow={setShow}
                                   product={product}
                               />
                               <EditModal
                                   brandOptions={brandOptions}
                                   show={showEdit}
                                   setShow={setShowEdit}
                                   product={product}
                                   refresh={setRefresh}
                               />
                               <CreateModal
                                   brandOptions={brandOptions}
                                   show={showCreate}
                                   setShow={setShowCreate}
                                   refresh={setRefresh}
                               />
                           </>
                            :
                            null}
                    </Col>
                </Row>
            </Container>
        </>
    );
}
export default ProductsIndex;

if (document.getElementById('example')) {
    ReactDOM.render(<ProductsIndex />, document.getElementById('example'));
}
