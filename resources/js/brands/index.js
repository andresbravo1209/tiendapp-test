import React, {useEffect, useState} from 'react';
import ReactDOM from 'react-dom';
import axios from "axios";
import {
    Button,
    ButtonGroup,
    Col,
    Container,
    Row,
    Table,
} from "react-bootstrap";
import NavBar from "../components/NavBar";
import moment from "moment/moment";
import {
    BsEyeFill,
    BsPencilSquare,
    BsXCircleFill
} from "react-icons/bs";
import swal from "sweetalert";
import CreateModal from "./create-modal";
import EditModal from "./edit-modal";

moment.locale('es');
const BrandsIndex = () => {

    const [show, setShow] = useState(false);
    const [refresh, setRefresh] = useState(false);
    const [showEdit, setShowEdit] = useState(false);
    const [showCreate, setShowCreate] = useState(false);
    const [brand, setBrand] = useState({});

    const handleShow = (e, product) => {
        e.preventDefault();
        setBrand(product);
        setShow(true)
    };

    const handleShowEdit = (e, product) => {
        e.preventDefault();
        setBrand(product);
        setShowEdit(true)
    };

    const [brands, setBrands] = useState([]);

    useEffect(() => {
        axios.get('/api/brands').then(
            response => {
                if (response.data.success) {
                    setBrands(response.data.data.brands)
                }
            });
    }, [refresh]);

    const DescriptionField = ({text}) => <td>{text}</td>;

    const onDelete = (e, brand) => {
        e.preventDefault();
        swal({
            title: "Borrar Marca?",
            text: `Estas a punto de borrar la marca ${brand.name}`,
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    axios.delete(`/api/brands/${brand.reference}`).then(response =>
                    {
                        if (response.data.success){
                            setRefresh(refresh => !refresh);
                            swal("La marca ha sido borrada!", {
                                icon: "success",
                            });
                        }
                    }).catch(error => {
                        //  Handling the response (Show an error notification etc)
                        if (error.response) { // You can also check the status i.e 422
                            swal("Ups!", {
                                icon: "error",
                                text: error.response.data.error.message,
                            });
                        }
                    });

                }
            });
    };

    return (
        <>
            <NavBar/>
            <Container fluid>
                <Row className='text-end mt-2'>
                    <Col>
                        <Button variant="primary" type='button' className='text-center' onClick={e => setShowCreate(true)}>
                            Crear Marca
                        </Button>
                    </Col>
                </Row>
                <Row className='text-center'>
                    <Col className='mt-5'>
                        {brands.length?
                            <>
                                <Table responsive striped bordered hover>
                                    <thead>
                                    <tr>
                                        <th>Referencia</th>
                                        <th>Nombre</th>
                                        <th>Acciones</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {brands.map(brand => (
                                        <tr key={brand.reference}>
                                            <DescriptionField text={brand.reference}/>
                                            <DescriptionField text={brand.name}/>
                                            <td>
                                                <ButtonGroup size="sm">
                                                    <Button onClick={e => handleShowEdit(e, brand)}>
                                                        <BsPencilSquare/>
                                                    </Button>
                                                    <Button onClick={e => onDelete(e, brand)}>
                                                        <BsXCircleFill/>
                                                    </Button>
                                                </ButtonGroup>
                                            </td>
                                        </tr>
                                    ))}
                                    </tbody>
                                </Table>
                                <EditModal
                                    show={showEdit}
                                    setShow={setShowEdit}
                                    brand={brand}
                                    refresh={setRefresh}
                                />
                                <CreateModal
                                    show={showCreate}
                                    setShow={setShowCreate}
                                    refresh={setRefresh}
                                />
                            </>
                            :
                            null}
                    </Col>
                </Row>
            </Container>
        </>
    );
}
export default BrandsIndex;

if (document.getElementById('example')) {
    ReactDOM.render(<BrandsIndex />, document.getElementById('example'));
}
