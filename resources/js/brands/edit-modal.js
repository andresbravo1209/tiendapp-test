import React, {useEffect} from 'react';
import {
    Button,
    Col,
    Modal,
    Row,
} from "react-bootstrap";
import Input from "../components/fields/Input";
import {useForm} from "react-hook-form";
import { yupResolver } from '@hookform/resolvers/yup';
import axios from "axios";
import swal from 'sweetalert';
import {brandsValidationSchema} from "../validations/brands";

const EditModal = ({show, setShow, brand, refresh = () => {}}) => {

    const handleClose = () => setShow(false);
    const { register, setValue, handleSubmit, formState: { errors }, } = useForm({
        resolver: yupResolver(brandsValidationSchema)
    });

    useEffect(() => {
        if (!brand) return;
        setValue('name', brand.name);
    }, [brand])

    const onSubmit = (data) => {
        axios.put(`/api/brands/${brand.reference}`, data).then(
            response => {
                if (response.data.success) {
                    refresh(refresh => !refresh);
                    handleClose();
                    swal("Exito!", "Marca Actualizada!", "success");
                }
            }).catch(error => {
            //  Handling the response
            if (error.response.data.errors){
                let msg = 'Error de validación';
                const errors = error.response.data.errors;
                msg += ': ' + Object.values(errors).join(' ');
                swal({
                    title: "Ops!",
                    text: msg,
                    icon: "warning",
                });
            }
        });
    };

    return (
        <>
            <Modal
                size='lg'
                show={show}
                onHide={handleClose}
                backdrop="static"
                keyboard={false}
            >
                <Modal.Header closeButton>
                    <Modal.Title><b>Editar Marca: </b> {brand.name}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <form>
                        <Row>
                            <Col>
                                <Input
                                    name='Nombre'
                                    label="Nombre de la marca"
                                    field='name'
                                    register={register}
                                    defaultValue={brand.name}
                                    required
                                />
                                {errors.name &&
                                <small className='text-danger'>{errors.name?.message}</small>}
                            </Col>
                        </Row>
                        <Row>
                            <Col className='text-center'>
                                <Button className='mt-2' type='submit' onClick={handleSubmit(onSubmit)}>
                                    Editar
                                </Button>
                            </Col>
                        </Row>
                    </form>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="primary" onClick={handleClose}>Cerrar</Button>
                </Modal.Footer>
            </Modal>
        </>
    );
}

export default EditModal;
