import * as yup from "yup";

export const brandsValidationSchema = yup.object().shape({
    name: yup.string().required('Nombre es un campo requerido'),
});
