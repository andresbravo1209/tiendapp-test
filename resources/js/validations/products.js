import * as yup from "yup";

export const productsValidationSchema = yup.object().shape({
    name: yup.string().required('Nombre es un campo requerido'),
    observations: yup.string().required('Observaciones es un campo requerido'),
    size: yup.string().test('size','Talla debe ser una opción valida', size => size !== ''),
    quantity: yup.number().positive().integer().required('Cantidad debe ser una cantidad mayor a 1.'),
    brand_id: yup.string().test('brand_id','Marca debe ser una opción valida', size => size !== ''),
});
