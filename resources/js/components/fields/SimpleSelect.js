import React from 'react';
import {FloatingLabel, Form} from "react-bootstrap";

const SimpleSelect = React.forwardRef(({ options = [], onChange, onBlur, name, label, required, register, defaultValue, field, defaultMessage = null}, ref) => (
    <>
        <FloatingLabel label={label}>
            <Form.Select defaultValue={defaultValue} name={name} className='form-control' aria-label="Floating label select example" {...register(field, { required })}>
                <option value=''>{defaultMessage}</option>
                {options.length > 0 && options.map(option => (
                    <option key={option.value} value={option.value}>{option.label}</option>
                ))}
            </Form.Select>
        </FloatingLabel>
    </>
));

export default SimpleSelect;
