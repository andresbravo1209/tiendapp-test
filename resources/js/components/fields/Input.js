import React from "react";
import {Form} from "react-bootstrap";

const Input = ({ defaultValue = null, name, field ,label, register, required }) => {
    return (
        <>
            {/*<label></label>*/}
            <Form.Label>{label}</Form.Label>
            <Form.Control
                {...register(field, { required })}
                className='form-control'
                size="sm"
                type="text"
                placeholder={`Ingresa ${name}`}
                defaultValue={defaultValue}
            />
            {/*<input className='form-control' {...register(field, { required })} />*/}
        </>
    );
};
export default Input;
