import React from 'react';
import {
    Container,
    Nav,
    Navbar,
} from "react-bootstrap";

const NavBar = () => {
    return (
        <Navbar bg="dark" variant="dark">
                <Container>
                    <Navbar.Brand href="/">TiendApp Test</Navbar.Brand>
                    <Nav className="me-auto">
                        <Nav.Link href="/brands">Marcas</Nav.Link>
                        <Nav.Link href="/products">Productos</Nav.Link>
                    </Nav>
                </Container>
            </Navbar>
    );
}

export default NavBar;

