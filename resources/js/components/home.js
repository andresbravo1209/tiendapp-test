import React from 'react';
import ReactDOM from 'react-dom';
import {
    Button,
    Card,
    Col,
    Container,
    Row,
} from "react-bootstrap";
import NavBar from "./NavBar";

const Home = () => {
    return (
        <>
            <NavBar/>
            <Container fluid>
                <Row className='text-center'>
                    <Col className='mt-5'>
                        <Card className="text-center">
                            <Card.Header>Marcas & Productos</Card.Header>
                            <Card.Body>
                                <Card.Title>Esta es una App de prueba.</Card.Title>
                                <Card.Text>
                                    Prueba tecnica de conocimiento TiendApp.
                                </Card.Text>
                                <Button variant="primary">Entendido.</Button>
                            </Card.Body>
                            <Card.Footer className="text-muted">Desarrollado por Andres Bravo.</Card.Footer>
                        </Card>
                    </Col>
                </Row>
            </Container>
        </>
    );
}

export default Home;

if (document.getElementById('example')) {
    ReactDOM.render(<Home />, document.getElementById('example'));
}
