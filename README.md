### Prueba tecnica de conocimiento TiendApp
App de conocimiento tecnico TiendApp desarrollado por Andres Bravo.

### Configuracion general del proyecto:

- ¡Es importante que la DB en MySQL tenga el nombre tiendapp-test !
- Clonar el repositorio.
- Copiar archivo de configuración .env.example en .env
- Instalar dependencias del proyecto  *composer install*
- Crear llave del proyecto *php artisan key:generate*

### Correr Seeders para obtener datos de prueba en la aplicación

1. *php artisan db:seed --class=BrandSeeder*
2. *php artisan db:seed --class=ProductSeeder*

### Configurar dependencias FRONTEND

1. Instalar dependencias de NPM *npm install*
2. Compicación de archivos JS *npm run dev | npm run prod*


### Configuración TESTS

1. La DB de pruebas/testing debe ser nombrada como *tiendapp-tests* [Ver archivo phpunit.xml]
2. Ejecutar *php artisan test --testdox* para validar la correcta ejecución de las pruebas unitarias a los endpoints desarrollados.

