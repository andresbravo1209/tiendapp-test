<?php

namespace Tests\Unit;

use App\Models\Brand;
use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class BrandsTest extends TestCase
{
    use RefreshDatabase;

    private $brandOne, $brandTwo;

    protected function setUp(): void
    {
        parent::setUp();
    }
    /**
     * @testdox  Test endpoint Listado de los marcas
     */
    public function testListBrands()
    {
        Brand::factory(3)->create();
        $response = $this->get('api/brands');

        $response->assertJson([
            'success' => true,
            'data' => [
                'brands' => []
            ]
        ]);

        $data = $response->json('data.brands');
        $count = Brand::all()->count();
        $this->assertCount($count, $data);
    }

    /**
     * @testdox Test endpoint creacion de los marcas
     */
    public function testCreateBrands()
    {
        $product = Brand::factory()->make();
        $data = [
            'name' => $product->name,
        ];
        $response = $this->post('api/brands', $data);
        $response->assertOk();
        $response->assertJson([
            'success' => true,
            'data' => [
                'message' => 'Marca creada!',
                'brand' => [],
            ]
        ]);

        $id = $response->json('data.brand.reference');
        $product = Brand::query()->findOrFail($id);
        $this->assertModelExists($product);

    }

    /**
     * @testdox Test endpoint edición de las marcas
     */
    public function testUpdateBrands()
    {
        $brand = Brand::factory()->create();
        $data = [
            'name' => 'Mi marca',
        ];

        $response = $this->put("api/brands/$brand->reference", $data);
        $response->assertOk();
        $response->assertJson([
            'success' => true,
            'data' => [
                'message' => 'Marca actualizada!',
                'brand' => [],
            ]
        ]);

        $reference = $response->json('data.brand.reference');
        $brand = Brand::query()->findOrFail($reference);
        $this->assertModelExists($brand);
        $this->assertEquals('Mi marca', $brand->name);
    }

    /**
     * @testdox Test endpoint eliminación de las marcas
     */
    public function testDeleteBrands()
    {
        $brand = Brand::factory()->create();

        $response = $this->delete("api/brands/$brand->reference");
        $response->assertOk();
        $response->assertJson([
            'success' => true,
            'data' => [
                'message' => 'Marca Eliminada!',
            ]
        ]);

        $count = Brand::all()->toArray();
        $this->assertCount(0, $count);
    }

    /**
     * @testdox Test endpoint eliminación de las marcas muestra error
     */
    public function testDeleteBrandsWithProducts()
    {
        $brand = Brand::factory()->create();
        Product::factory()->create(['brand_id' => $brand->reference]);

        $response = $this->delete("api/brands/$brand->reference");
        $response->assertStatus(400);
        $response->assertJson([
            'success' => false,
            'error' => [
                'message' => 'La marca tiene productos asociados.',
                'error' => true,
            ]
        ]);

    }
}
