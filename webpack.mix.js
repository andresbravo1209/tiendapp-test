const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .react()
    .sass('resources/sass/app.scss', 'public/css');

mix.js('resources/js/components/home.js', 'public/js/components')
    .react();

mix.js('resources/js/products/index.js', 'public/js/products')
    .react();

mix.js('resources/js/brands/index.js', 'public/js/brands')
    .react();
