<?php

namespace App\tiendApp\Repositories;

use App\Base\BaseRepository;
use App\Models\Brand;

class BrandRepository extends BaseRepository
{
    public function getModel(): Brand
    {
        return new Brand;
    }

}
