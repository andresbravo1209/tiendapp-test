<?php

namespace App\tiendApp\Repositories;

use App\Base\BaseRepository;
use App\Models\Product;
use Illuminate\Database\Eloquent\Model;

class ProductRepository extends BaseRepository
{
    public function getModel(): Product
    {
        return new Product;
    }

}
