<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductStoreRequest;
use App\Http\Requests\ProductUpdateRequest;
use App\Models\Product;
use App\tiendApp\Repositories\ProductRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\View\View;
use Symfony\Component\HttpFoundation\Response as HttpResponse;
use Exception;

class ProductController extends Controller
{
    private $products;

    public function __construct(ProductRepository $productRepository)
    {
        $this->products = $productRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index(): View
    {
        return view('products.index');
    }

    /**
     * Return a listing of the resource.
     *
     * @return JsonResponse
     */
    public function listIndex(): JsonResponse
    {
        $products = $this->products
            ->getModel()
            ->with('brand')
            ->get();

        return response()->success(
            compact('products')
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ProductStoreRequest $request
     * @return JsonResponse
     */
    public function store(ProductStoreRequest $request): JsonResponse
    {
        try{
            $data = $request->validated();
            $data['embark_at'] = now();
            $product = $this->products->create($data);
        }catch (Exception $exception){
            return response()->error([
                'message' => 'Error creando producto',
                'error' => $exception->getMessage(),
            ], HttpResponse::HTTP_BAD_REQUEST);
        }

        return response()->success([
            'message' => 'Producto creado!',
            'product' => $product
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ProductUpdateRequest $request
     * @param $id
     * @return JsonResponse
     */
    public function update(ProductUpdateRequest $request, $id): JsonResponse
    {
        try{
            $product = $this->products->findOrFail($id);
            $data = $request->all();
            $data['embark_at'] = now();
        }catch (Exception $exception){
            return response()->error([
                'message' => 'Producto no encontrado',
                'error' => $exception->getMessage(),
            ], HttpResponse::HTTP_BAD_REQUEST);
        }

        $product->update($data);

        return response()->success([
            'message' => 'Producto actualizado!',
            'product' => $product
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return JsonResponse
     */
    public function destroy($id): JsonResponse
    {
        try{
            /**@var Product $product*/
            $product = $this->products->findOrFail($id);
            $this->products->delete($product);
        }catch (Exception $exception){
            return response()->error([
                'message' => 'Producto no encontrado',
                'error' => $exception->getMessage(),
            ], HttpResponse::HTTP_BAD_REQUEST);
        }

        return response()->success([
            'message' => 'Producto Eliminado!',
        ]);
    }
}
