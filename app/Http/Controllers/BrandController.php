<?php

namespace App\Http\Controllers;

use App\Http\Requests\BrandStoreRequest;
use App\Http\Requests\BrandUpdateRequest;
use App\Models\Brand;
use App\Models\Product;
use App\tiendApp\Repositories\BrandRepository;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\View\View;
use Symfony\Component\HttpFoundation\Response as HttpResponse;
use Exception;


class BrandController extends Controller
{
    private $brands;

    public function __construct(BrandRepository $brandRepository)
    {
        $this->brands = $brandRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index(): View
    {
        return view('brands.index');
    }

    /**
     * Return a listing of the resource.
     *
     * @return JsonResponse
     */
    public function listIndex(): JsonResponse
    {
        $brands = $this->brands->all();
        return response()->success(
            compact('brands')
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param BrandStoreRequest $request
     * @return JsonResponse
     */
    public function store(BrandStoreRequest $request): JsonResponse
    {
        try{
            $data = $request->validated();
            $brand = $this->brands->create($data);
        }catch (Exception $exception){
            return response()->error([
                'message' => 'Error creando marca',
                'error' => $exception->getMessage(),
            ], HttpResponse::HTTP_BAD_REQUEST);
        }

        return response()->success([
            'message' => 'Marca creada!',
            'brand' => $brand
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param Brand $brand
     * @return Response
     */
    public function show(Brand $brand)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Brand $brand
     * @return Response
     */
    public function edit(Brand $brand)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param BrandUpdateRequest $request
     * @param $reference
     * @return JsonResponse
     */
    public function update(BrandUpdateRequest $request, $reference): JsonResponse
    {
        try{
            $brand = $this->brands->getModel()->where('reference', $reference)->firstOrFail();
            $data = $request->all();
        }catch (Exception $exception){
            return response()->error([
                'message' => 'Producto no encontrado',
                'error' => $exception->getMessage(),
            ], HttpResponse::HTTP_BAD_REQUEST);
        }

        $brand->update($data);

        return response()->success([
            'message' => 'Marca actualizada!',
            'brand' => $brand
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $reference
     * @return JsonResponse
     */
    public function destroy($reference): JsonResponse
    {
        try{
            /**@var Brand $brand*/
            $brand = $this->brands->getModel()->where('reference', $reference)->firstOrFail();
        }catch (Exception $exception){
            return response()->error([
                'message' => 'Marca no encontrada',
                'error' => $exception->getMessage(),
            ], HttpResponse::HTTP_BAD_REQUEST);
        }

        if ($brand->products()->exists()){
            return response()->error([
                'message' => 'La marca tiene productos asociados.',
                'error' => true,
            ], HttpResponse::HTTP_BAD_REQUEST);
        }

        $this->brands->delete($brand);

        return response()->success([
            'message' => 'Marca Eliminada!',
        ]);
    }
}
