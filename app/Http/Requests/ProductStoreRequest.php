<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'size' => 'required|string',
            'observations' => 'required|string',
            'quantity' => 'required|numeric',
            'brand_id' => 'required|string',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'El campo nombre es requerido',
            'size.required' => 'El campo talla es requerido',
            'observations.required' => 'El campo observaciones es requerido',
            'quantity.required' => 'El campo cantidad es requerido',
            'brand_id.required' => 'El campo marca es requerido',
        ];
    }
}
