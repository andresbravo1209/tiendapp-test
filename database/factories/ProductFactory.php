<?php

namespace Database\Factories;

use App\Models\Brand;
use App\Models\Product;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $brands = Brand::all()->pluck('reference');
        return [
            'name' => $this->faker->name,
            'size' => $this->faker->randomElement(['S', 'M', 'L', 'XL']),
            'observations' => $this->faker->lexify('????????????????'),
            'quantity' => $this->faker->numberBetween(1, 20),
            'embark_at' => now(),
            'brand_id' => (string) $this->faker->randomElement($brands),
        ];
    }
}
