<?php

namespace Database\Seeders;

use App\Models\Brand;
use App\Models\Product;
use Faker\Factory;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        $brands = Brand::all()->pluck('reference');

        Product::factory()->create([
            'brand_id' => $faker->randomElement($brands),
        ]);

        Product::factory()->create([
            'brand_id' => $faker->randomElement($brands),
        ]);
    }
}
